from typing import Any, Tuple

import numpy as np
import pandas as pd


def clean_wine_dataset(wine_data: Any) -> Tuple[pd.DataFrame, pd.Series]:
    """
    Cleans the Wine dataset by removing rows with missing values and separates features from the target.

    Args:
        wine_data: Raw wine dataset from sklearn.datasets.

    Returns:
        Cleaned features and target.
    """

    # Unpack the data and target
    X: np.ndarray = wine_data.data
    y: np.ndarray = wine_data.target

    # Convert to pandas DataFrame
    df: pd.DataFrame = pd.DataFrame(X, columns=wine_data.feature_names)
    df["target"] = y

    # Remove rows with missing values
    df_cleaned: pd.DataFrame = df.dropna()

    # Separate features and target after cleaning
    X_cleaned: pd.DataFrame = df_cleaned.drop("target", axis=1)
    y_cleaned: Any = df_cleaned["target"]

    # Assert its a series, as I'm working with types
    assert isinstance(y_cleaned, pd.Series)

    return X_cleaned, y_cleaned
