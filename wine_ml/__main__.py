from typing import Any

from sklearn.datasets import load_wine
from sklearn.pipeline import Pipeline

from clean_dataset import clean_wine_dataset
from model import create_pipeline, train_and_evaluate


def main() -> None:
    wine: Any = load_wine()

    X_cleaned, y_cleaned = clean_wine_dataset(wine)

    pipeline: Pipeline = create_pipeline()

    train_and_evaluate(pipeline, X_cleaned, y_cleaned, wine)


if __name__ == "__main__":
    main()
