from typing import Any

import numpy as np
import pandas as pd
import xgboost as xgb
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler


def create_pipeline() -> Pipeline:
    """
    Creates a machine learning pipeline with a standard scaler and XGBoost classifier.

    Returns:
        Sklearn pipeline.
    """

    return Pipeline(
        [
            ("scaler", StandardScaler()),
            ("xgb", xgb.XGBClassifier(use_label_encoder=False, eval_metric="mlogloss")),
        ]
    )


def train_and_evaluate(
    pipeline: Pipeline, X: pd.DataFrame, y: pd.Series, wine: Any
) -> None:
    """
    Trains the pipeline on the training data and evaluates it on the test data.

    Args:
        pipeline: Sklearn pipeline.
        X: Feature data.
        y: Target data.
        wine: Raw wine dataset for target names.
    """

    # Split the data
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=42, stratify=y
    )

    # Train the model using the pipeline
    pipeline.fit(X_train, y_train)

    # Make predictions on the test set
    y_pred = pipeline.predict(X_test)

    # Evaluate the performance
    accuracy: float = accuracy_score(y_test, y_pred)
    conf_matrix: np.ndarray = confusion_matrix(y_test, y_pred)
    class_report: Any = classification_report(
        y_test, y_pred, target_names=wine.target_names
    )

    print(
        f"Accuracy: {accuracy:.2f}\n"
        + "Confusion Matrix:\n"
        + f"{conf_matrix}\n"
        + "Classification Report:\n"
        + f"{class_report}"
    )
