with import <nixpkgs> {};
  stdenv.mkDerivation {
    name = "python-env";
    buildInputs = [
      python311
      python311Packages.scikit-learn
      python311Packages.xgboost
      python311Packages.pandas
    ];
  }
